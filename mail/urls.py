from django.urls import path
from .views import emailView, load_cadena, successView, uploadCadena
from django.views.generic import TemplateView

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html")),
    path('sendmail', emailView),
    path('success/', successView, name='success'),
    path('cadenaInstitucional/', load_cadena, name='cadena'),
    path('ucadenaInstitucional/', uploadCadena),
]
