from django.http import HttpResponse
from django.core.mail import BadHeaderError, send_mail
from django.shortcuts import render, redirect
from .forms import ContactForm
from config.celery import app
from .mssql import insertCadena


def emailView(request):
    #form = ContactForm(request.POST)
    #if form.is_valid():
    print(request.GET)
    subject = request.GET.get('subject')
    from_email = 'soporte@apci.gob.pe'#form.cleaned_data['email']
    message = request.GET.get('message')
    try:
        send_mail(subject, message, from_email, [request.GET.get('email')],fail_silently=False,)

    except BadHeaderError:
        return HttpResponse('Inválido, error.')
    return redirect('success')
    #return render(request, "index.html", {'form': form})

def successView(request):
    return HttpResponse('Correcto! Gracias por tu mensaje.')


def load_cadena(request):
    return render(request, "cadenaInstitucional.html")

def join_list(di):
    for ic in di.keys():
        li = []
        for id in range(len(di[ic])):
            dd = {}
            for ic in di.keys():
                dd[ic] = di[ic][id]
            li.append(dd)
        break
    return li

def removePoint(string):
    if string.find('.')>0:
        string = string[string.find('.') + 1:len(string)].strip()
    return string

def clist(d,a):
    b = False
    if not d:
        return b

    for i in d:
        if a in i:
            b = True
    return b

def addLevel(a,li,u):
    if not li:
        for i in li:
            if a in i:
                i[a].append(u)
    return li


def processE(a, u, li):
    nn = removePoint(u[a])
    if not len(li)==0:
        if clist(li, nn):
            if a in u: del u[a]
            li = addLevel(nn, li, u)
        else:
            if a in u: del u[a]
            li.append(nn)
    else:
        if a in u: del u[a]
        li.append(nn)

    return li


def processP(a, u, li):
    nn = removePoint(u[a])
    if not len(li)==0:
        if clist(li, nn):
            ng = [u]
            if "EJECUTORA" in u:
                ns = removePoint(u[a])
                ng = processE('EJECUTORA', u, updateLevel(ns, li))
                if a in u: del u[a]

            if a in u: del u[a]
            li = addLevel(nn, li, ng)
        else:
            if a in u: del u[a]
            ng = [u]
            if "EJECUTORA" in u:
                ng = processE('EJECUTORA', u, [])
            li.append({nn: ng})
    else:
        if a in u: del u[a]
        ng = [u]
        if "EJECUTORA" in u:
            ng = processE('EJECUTORA', u, [])
        li.append({nn: ng})

    return li

def processS(a,u,li):
    nn = removePoint(u[a])
    if not len(li)==0:
        if clist(li,nn):
            ng = [u]
            if "PLIEGO" in u:
                ns = removePoint(u[a])
                ng = processP('PLIEGO',u,updateLevel(ns,li))
                if a in u: del u[a]

            if a in u: del u[a]
            li = addLevel(nn, li, ng)
        else:
            if a in u: del u[a]
            ng = [u]
            if "PLIEGO" in u:
                ng = processP('PLIEGO', u, [])
            li.append({nn: ng})
    else:
        if a in u: del u[a]
        ng = [u]
        if "PLIEGO" in u:
            ng = processP('PLIEGO', u, [])
        li.append({nn: ng})

    return li

def processLevel(a,u,li):
    nn = removePoint(u[a])
    if not len(li)==0:
        if clist(li,nn):
            ng = [u]
            if "SECTOR" in u:
                ns = removePoint(u[a])
                ng = processS('SECTOR',u,updateLevel(ns,li))
                if a in u: del u[a]

            if a in u: del u[a]
            li = addLevel(nn, li, ng)
        else:
            if a in u: del u[a]
            ng = [u]
            if "SECTOR" in u:
                ng = processS('SECTOR', u, [])

            li.append({nn: ng})

    else:
        if a in u: del u[a]
        ng = [u]
        if "SECTOR" in u:
            ng = processS('SECTOR', u, [])

        li.append({nn: ng})

    return li

def updateLevel(a,li):
    for i in li:
        if a in i:
            return i[a]

def generateQuery(li):
    so = '0000000'
    co = '00000'
    do = '00'

    for i in li:
        anio = [j for j in i.keys()][0] #extraer años
        cont = 1
        for a in i[anio]:
            na = str(cont) + so
            level = [b for b in a.keys()][0] #extraer nivel de gobierno
            #print("nivel de gobierno: "+ na + " AÑO: " +anio+" Nombre: "+level)
            insertCadena(anio, na, level)
            clev = 1
            for c in a[level]:
                sector = [d for d in c.keys()][0] #extraer sector
                ids = str( int(na) + int(str(clev+10)+co))  #id sector
                #print("sector: " + ids + " AÑO: " +anio + " Nombre: " + sector)
                insertCadena(anio, ids, sector)
                cpli = 1
                for e in c[sector]:
                    pliego = [f for f in e.keys()][0]  #extraer Pliego
                    idp = str(int(ids) + int(str(cpli+110)+do)) #id pliego
                    #print("pliego: " + idp + " AÑO: " + anio + " Nombre: " + pliego)
                    insertCadena(anio, idp, pliego)
                    cej = 1
                    for g in e[pliego]:
                        #extraer Ejecutora
                        ide = str(int(idp) + 10+cej)  # id ejecutora
                        #print("ejecutora: " + ide + " AÑO: " + anio + " Nombre: " + g)
                        insertCadena(anio, ide, g)
                        cej+=1
                    cpli+=1
                clev+=1
            cont+=1



def uploadCadena(request):
        filehandle = request.FILES['file']
        di = dict(filehandle.get_dict(sheet_name=request.POST.get('hoja')))
        j = join_list(di)
        nlist = []
        count = 0
        for u in j:
            a = str(u['AÑO'])
            if clist(nlist,a):
                if a in u: del u[a]
                nlist= addLevel(a,nlist,processLevel('NIVEL_GOBIERNO',u,updateLevel(a,nlist)))
            else:
                if a in u: del u[a]
                ng = processLevel('NIVEL_GOBIERNO',u,[])
                nlist.append({a: ng})
            count+=1
            if count == 3:
                pass#break

        print('Filas: ',count)
        print(nlist)
        generateQuery(nlist)
        return HttpResponse("correcto")
